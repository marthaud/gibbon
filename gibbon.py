def sons_dict_and_list(tree):
    if type(tree) is dict:
        return tree.items()
    elif type(tree) is list:
        return ((str(i), tree[i]) for i in range(len(tree)))
    else:
        return []


def gen_on_paths(
    tree,
    paths,
    sons=sons_dict_and_list,
    ancestor=[],
    qall="*",
    retempty=False,
    delimiter=" ",
):
    """Return an generator on "tree" subtrees located in "paths".

    In "paths", each node are represent by a token (string). "tree" sons and 
    their token are gived by the "sons" function provided by user. 
    
    Arguments:
        tree -- The tree.

        paths {string list, string list list, dict} -- "paths" has three 
        	possible format:
            - a list of path represented by string. ex :
                ["this is a path", "this is an other path"]
            - a list of path represented by list of tocken.ex :
                [["this","is","a","path"], ["this","is","an","other","path"]]
            - a tree of dict. Keys are token of node sons.ex :
                {"this":{"is":{"a":{"path":{}},"an":{"other":{"path":{}}}}}}
    
    Keyword Arguments:
        sons {tree -> (string, tree) gen} -- Function. Return a generator of
        	tree sons and their token. (default: {sons_dict_and_list})

        ancestor {list} -- If the tree is a subtree, this is the subpath to
        	him. (default: {[]})

        qall {str} -- Quantificateur all token. In a path this token indicated 
        	that to include all sons in the path. (default: {"*"})

        retempty {bool} -- Return empty(None) subtrees located in "path". 
        	(default: {False})

        delimiter {str} -- The delimiter incase of paths are strings. 
        	(default: {" "})
    """
    treepaths = path_parsor(paths, delimiter=delimiter)
    constant_parameter = {"sons": sons, "qall": qall, "retempty": retempty}
    if tree == None and not retempty:
        return
    if empty_dict(treepaths):
        yield tree, ancestor
    else:
        for token, subpaths in treepaths.items():
            for idson, son in sons(tree):
                if token in (qall, idson):
                    yield from gen_on_paths(
                        son,
                        subpaths,
                        ancestor=ancestor + [idson],
                        **constant_parameter
                    )


def path_parsor(paths, **kwargs):
    typepaths = type(paths)
    if typepaths is dict:
        return paths
    if typepaths is list:
        if paths == []:
            return dict()
        typepath = type(paths[0])
        if typepath is list:
            return pathlist_parsor(paths, **kwargs)
        if typepath is str:
            return pathstring_parsor(paths, **kwargs)
    raise TypeError("paths has not a good type : see doc")


def pathstring_parsor(pathstringlist, delimiter=" ", **kwargs):
    pathlistgen = [
        pathstring.split(delimiter) for pathstring in pathstringlist
    ]
    pathlistgen = list(filter(lambda s: s != [""], pathlistgen))
    return pathlist_parsor(pathlistgen, **kwargs)


def pathlist_parsor(pathlistgen, **kwargs):
    paths = dict()
    if pathlistgen == []:
        return dict()
    pathlistgen = filter(lambda l: l != [], pathlistgen)
    for pathlist in pathlistgen:
        token, *subpathlist = pathlist
        if token not in paths:
            paths[token] = []
        if subpathlist != []:
            paths[token].append(subpathlist)
    for token, subpathlists in paths.items():
        paths[token] = pathlist_parsor(subpathlists)
    return paths


def empty_dict(d):
    return type(d) == dict and not d
