import pytest
import yaml
import os

import gibbon

HERE_DIR = os.path.dirname(os.path.abspath(__file__))


@pytest.fixture
def data_graph():
    with open(HERE_DIR + "/data/graph.yml") as f:
        return yaml.load(f, Loader=yaml.FullLoader)


class TestGenOnPath:
    def test_nominal_case(self, data_graph):
        paths = ["graph * transition * name", "graph * transition * flags"]
        expected_result = [
            ("node2", ["graph", "0", "transition", "0", "name"]),
            (["F1"], ["graph", "0", "transition", "0", "flags"]),
            ("node3", ["graph", "1", "transition", "0", "name"]),
            (["F2"], ["graph", "1", "transition", "0", "flags"]),
            ("node4", ["graph", "2", "transition", "0", "name"]),
            (["F3", 0], ["graph", "2", "transition", "0", "flags"]),
            ("node2", ["graph", "3", "transition", "0", "name"]),
            ("node5", ["graph", "3", "transition", "1", "name"]),
            (["F4"], ["graph", "3", "transition", "1", "flags"]),
        ]
        result = list(gibbon.gen_on_paths(data_graph, paths))
        assert len(result) == len(expected_result)
        for element in result:
            assert element in expected_result

    def test_qall_on_dict(self, data_graph):
        paths = ["graph 0 transition 0 *"]
        expected_result = [
            ("node2", ["graph", "0", "transition", "0", "name"]),
            ("green", ["graph", "0", "transition", "0", "color"]),
            (["F1"], ["graph", "0", "transition", "0", "flags"]),
        ]
        result = list(gibbon.gen_on_paths(data_graph, paths))
        assert len(result) == len(expected_result)
        for element in result:
            assert element in expected_result

    def test_one_path(self, data_graph):
        paths = ["graph 3 transition 1 name"]
        expected_result = [
            ("node5", ["graph", "3", "transition", "1", "name"])
        ]
        result = list(gibbon.gen_on_paths(data_graph, paths))
        assert expected_result == result

    def test_noneresulte(self, data_graph):
        paths = ["graph 3 transition 0 flags"]
        result1 = list(gibbon.gen_on_paths(data_graph, paths, retempty=True))
        result2 = list(gibbon.gen_on_paths(data_graph, paths))
        assert result1 == [(None, ["graph", "3", "transition", "0", "flags"])]
        assert result2 == []

    def test_false_path(self, data_graph):
        paths = [
            "graph 3 transition 1 name falsekey",
            "graph 6 transition * name",
            "lapin",
        ]
        result = list(gibbon.gen_on_paths(data_graph, paths))
        assert result == []

    def test_empty_path(self, data_graph):
        paths1 = []
        paths2 = [""]
        expected_result = [
            (
                {
                    "graph": [
                        {
                            "name": "node1",
                            "transition": [
                                {
                                    "name": "node2",
                                    "color": "green",
                                    "flags": ["F1"],
                                }
                            ],
                        },
                        {
                            "name": "node2",
                            "transition": [
                                {
                                    "name": "node3",
                                    "color": "blue",
                                    "flags": ["F2"],
                                }
                            ],
                        },
                        {
                            "name": "node3",
                            "transition": [
                                {
                                    "name": "node4",
                                    "color": "green",
                                    "flags": ["F3", 0],
                                }
                            ],
                        },
                        {
                            "name": "node4",
                            "transition": [
                                {
                                    "name": "node2",
                                    "color": "green",
                                    "flags": None,
                                },
                                {
                                    "name": "node5",
                                    "color": "green",
                                    "flags": ["F4"],
                                },
                            ],
                        },
                        {"name": "node5"},
                    ]
                },
                [],
            )
        ]
        result1 = list(gibbon.gen_on_paths(data_graph, paths1))
        result2 = list(gibbon.gen_on_paths(data_graph, paths2))
        assert result1 == expected_result
        assert result2 == expected_result


class TestPathParsing:
    def test_nominal_case(self):
        pathstringlist = [
            "graph * transition * name",
            "graph * transition * flags",
        ]
        paths = {
            "graph": {"*": {"transition": {"*": {"name": {}, "flags": {}}}}}
        }
        assert paths == gibbon.path_parsor(pathstringlist)

    def test_delimiter(self):
        pathstringlist = [
            "graph.*.transition.*.name",
            "graph.*.transition.*.flags",
        ]
        paths = {
            "graph": {"*": {"transition": {"*": {"name": {}, "flags": {}}}}}
        }
        assert paths == gibbon.path_parsor(pathstringlist, delimiter=".")

    def test_list_of_list(self):
        pathstringlist = [
            ["graph", "*", "transition", "*", "name"],
            ["graph", "*", "transition", "*", "flags"],
        ]
        paths = {
            "graph": {"*": {"transition": {"*": {"name": {}, "flags": {}}}}}
        }
        assert paths == gibbon.path_parsor(pathstringlist, delimiter=" ")

    def test_tow_same_path(self):
        pathstringlist = [
            "graph * transition * name",
            "graph * transition * name",
            "graph * transition * flags",
        ]
        paths = {
            "graph": {"*": {"transition": {"*": {"name": {}, "flags": {}}}}}
        }
        assert paths == gibbon.path_parsor(pathstringlist)

    def test_emptypath(self):
        assert {} == gibbon.path_parsor([])
        assert {} == gibbon.path_parsor([""])
        assert {} == gibbon.path_parsor([[]])
        assert {} == gibbon.path_parsor(["", ""])
